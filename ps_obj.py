import re
from numbers import Number
import epics
import logging
import datetime
from epics import PV
import json
import os
import subprocess
import time

class PowerSupply:

    def __init__(self, device_access_name='HMG2', set_voltage=0.0, set_current=0.0, n_of_chn = 4, config_file = None):
        # Class self.logger config
        self.logger = logging.getLogger(self.__class__.__name__)
        log_file_name = 'power_supply_logger.log'
        logging.basicConfig(filename=log_file_name, format='%(asctime)s %(levelname)s %(message)s')
        self.logger.setLevel(logging.DEBUG)

        self.device = device_access_name
        self.n_of_channels = n_of_chn
        self.set_voltage = set_voltage
        self.set_current = set_current

        self.operation_mode = None

        if config_file is not None:
            with open(config_file, "r") as json_file:
                loaded_data = json.load(json_file)

            if "epics_device" in loaded_data["power_supply"]:
                self.operation_mode = "epics"
                self.logger.debug(f"Power Supply will be operated as: {self.operation_mode}")
                self.device = loaded_data["power_supply"]["epics_device"]

                self.set_voltage = loaded_data["power_supply"]["voltage"]
                self.set_current = loaded_data["power_supply"]["current"]
                self.n_of_channels = loaded_data["power_supply"]["n_of_channels"]

            elif "ssh_device" in loaded_data["power_supply"]:
                self.operation_mode = "ssh"
                self.logger.debug(f"Power Supply will be operated as: {self.operation_mode}")

                self.device = loaded_data["power_supply"]["ssh_device"]

                self.channels = {}
                for ch in loaded_data["power_supply"]["channels"]:
                    self.channels[ch] = loaded_data["power_supply"]["channels"][ch]

                self.n_of_channels  = len(self.channels)
                self.config = loaded_data["power_supply"]["config"]


    def __str__(self):
        return ("PowerSupply: {}\nNumber of Channels: {}\nV_set: {}\nI_set: {}\n\n".format(self.device, self.n_of_channels,self.set_voltage, self.set_current))

    def sub_command_run(self,command):
        command_output = None
        try:
            self.logger.debug("Trying to: {}".format(command))
            command_output = subprocess.check_output(
                command,
                shell=True)
            return command_output.decode('utf-8').strip()

        except subprocess.CalledProcessError:
            print("Failed when trying: {}".format(command))


    def channel_config(self, sel_chn):
        ch = list(self.channels.keys())[sel_chn] if isinstance(sel_chn, Number) else sel_chn
        for cfg, type in self.config:
            command = "{} 'snmpset -v 2c -m +WIENER-CRATE-MIB -c guru' {} {}.{} {} {}".format(self.device, self.channels[ch]['ip'], cfg ,self.channels[ch]['ch'], type , self.channels[ch][cfg])
            self.sub_command_run(command)

    def config_all(self):
        for channel in range(self.n_of_channels):
            self.channel_config(channel)

    def power_on(self, sel_chn=0):
        self.power_channel(state=True, sel_chn=sel_chn)

    def power_off(self, sel_chn=0):
        self.power_channel(state=False, sel_chn=sel_chn)

    def power_channel(self, state=True, sel_chn=0, raise_wait_time=10):
        if self.operation_mode == "epics":
            self.logger.debug("Powering channel {}: {}".format(sel_chn+1, state))
            PV("{}:CH:{}:setOn".format(self.device, sel_chn+1), auto_monitor=True).put(value=state, wait=True)
            PV("{}:OUTPUT:setOn".format(self.device), auto_monitor=True).put(value=state, wait=True)

        if self.operation_mode == "ssh":
            ch = list(self.channels.keys())[sel_chn] if isinstance(sel_chn, Number) else sel_chn
            command = "{} 'snmpset -v 2c -m +WIENER-CRATE-MIB -c guru' {} outputSwitch.{} i {}".format(self.device, self.channels[ch]['ip'], self.channels[ch]['ch'], int(state))
            self.sub_command_run(command)

            time.sleep(raise_wait_time)


    def power_all(self, state=True):
        for channel in range(self.n_of_channels):
            self.power_channel(state=state, sel_chn=channel, raise_wait_time=0)
        time.sleep(10)

    def set_channel_voltage(self, voltage=0, sel_chn=all):
        if self.operation_mode == "epics":
            if sel_chn == all:
                for channel in range(1,self.n_of_channels+1):
                    self.logger.debug("Setting CH_{}_V: {}".format(channel, voltage))
                    _ = PV("{}:CH:{}:Vset".format(self.device, channel)).put(value=voltage, wait=True)
            else:
                self.logger.debug("Setting CH_{}_V: {}".format(sel_chn, voltage))
                _ = PV("{}:CH:{}:Vset".format(self.device, sel_chn)).put(value=voltage, wait=True)

    def set_channel_current(self, current=0, sel_chn=all):
        if self.operation_mode == "epics":
            if sel_chn == all:
                for channel in range(1,self.n_of_channels+1):
                    self.logger.debug("Setting CH_{}_I: {}".format(channel, current))
                    _ = PV("{}:CH:{}:Iset".format(self.device, channel)).put(value=current, wait=True)
            else:
                self.logger.debug("Setting CH_{}_I: {}".format(sel_chn, current))
                _ = PV("{}:CH:{}:Iset".format(self.device, sel_chn)).put(value=current, wait=True)

    """ By default the method will set voltages and currents for all the channels
        from the initialized values when the object is created. """
    def set_iv(self, current=None, voltage=None, channel=all):
        if self.operation_mode == "epics":
            if current is not None and voltage is not None:
                self.set_channel_current(current, channel)
                self.set_channel_voltage(voltage, channel)
            else:
                for chn in range(self.n_of_channels):
                    self.set_channel_current(self.set_current[chn], chn+1)
                    self.set_channel_voltage(self.set_voltage[chn], chn+1)

    def get_iv(self, sel_chn=1):
        if self.operation_mode == "epics":
            current = PV("{}:CH:{}:Imom".format(self.device, sel_chn)).value
            voltage = PV("{}:CH:{}:Vmom".format(self.device, sel_chn)).value
            return [current, voltage]

        if self.operation_mode == "ssh":
            ch = list(self.channels.values())[sel_chn] if isinstance(sel_chn, Number) else sel_chn
            voltage_sen = self.sub_command_run(
                "{} 'snmpget -v 2c -m +WIENER-CRATE-MIB -c guru {} outputMeasurementSenseVoltage.{}  | grep -Eo \"[0-9]+([.][0-9]+)\"'".
                format(self.device, self.channels[ch]['ip'], self.channels[ch]['ch']))

            voltage_ter = self.sub_command_run(
                "{} 'snmpget -v 2c -m +WIENER-CRATE-MIB -c guru {} outputMeasurementTerminalVoltage.{} | grep -Eo \"[0-9]+([.][0-9]+)\"'".
                format(self.device, self.channels[ch]['ip'], self.channels[ch]['ch']))
            current = self.sub_command_run(
                "{} 'snmpget -v 2c -m +WIENER-CRATE-MIB -c guru {} outputMeasurementCurrent.{}  | grep -Eo \"[0-9]+([.][0-9]+)\"'".
                format(self.device, self.channels[ch]['ip'], self.channels[ch]['ch']))

            return [float(current), float(voltage_sen)]

# Example Usage
# if __name__ == "__main__":
#     ps = PowerSupply(config_file="module_test_config.json")
#     ps.config_all()

#     ps.power_all(True)

#     for ch in ps.channels:
#         print(ps.get_iv(ch))

#     ps.power_all(False)


