#!/usr/bin/env python
import subprocess
import logging
import time

# Set initial values for MPOD LV via SNMP

# 2014-12-09 DE: adapted to automatic configuration generator
# 2012-07-16 DE: add outputUserConfig (sense, regulation) for firmware v1909
# 2012-03-23 DE: adapted to mpod01 with u001 channel naming
# 2011-09-01 DE: adapt for mpod LV
# 2009-11-10 TD: change for Oct 2009 MIB file (change U->u)

# Class self.logger config
logger = logging.getLogger(__name__)
log_file_name = 'power_supply_logger.log'
logging.basicConfig(filename=log_file_name, format='%(asctime)s %(levelname)s %(message)s')
logger.setLevel(logging.DEBUG)

# Commands to query PSUs
snmpset = "ssh cbm@cbmflib01 'snmpset -v 2c -m +WIENER-CRATE-MIB -c guru'"
snmpget = "ssh cbm@cbmflib01 'snmpget -v 2c -m +WIENER-CRATE-MIB -c guru'"

# Channel definitions
channels = {
    'emu': {
        'ip': '10.203.0.62',
        'ch': 'u800',
        'outputSupervisionMaxCurrent'           :  4.01,
        'outputSupervisionMinSenseVoltage'      :  0.00,
        'outputSupervisionMaxSenseVoltage'      : 15.00,
        'outputSupervisionMaxTerminalVoltage'   :  8.50,
        'outputSupervisionMaxPower'             : 50.00,
        'outputVoltage'                         :  7.00,
        'outputCurrent'                         :  4.00,
        'outputSupervisionBehavior'             :  1365,
        'outputVoltageRiseRate'                 :  100,
        'outputVoltageFallRate'                 :  100,
        'outputUserConfig'                      :     8
    },
    'feb_n_side_ldo_1_2': {
        'ip': '10.203.0.62',
        'ch': 'u700',
        'outputSupervisionMaxCurrent'           :  4.01,
        'outputSupervisionMinSenseVoltage'      :  0.00,
        'outputSupervisionMaxSenseVoltage'      : 15.00,
        'outputSupervisionMaxTerminalVoltage'   :  8.50,
        'outputSupervisionMaxPower'             : 50.00,
        'outputVoltage'                         :  3.00,
        'outputCurrent'                         :  4.00,
        'outputSupervisionBehavior'             :  1365,
        'outputVoltageRiseRate'                 :  100,
        'outputVoltageFallRate'                 :  100,
        'outputUserConfig'                      :     8
    },
    'feb_n_side_ldo_1_8': {
        'ip': '10.203.0.62',
        'ch': 'u701',
        'outputSupervisionMaxCurrent'           :  4.01,
        'outputSupervisionMinSenseVoltage'      :  0.00,
        'outputSupervisionMaxSenseVoltage'      : 15.00,
        'outputSupervisionMaxTerminalVoltage'   :  8.50,
        'outputSupervisionMaxPower'             : 50.00,
        'outputVoltage'                         :  3.00,
        'outputCurrent'                         :  4.00,
        'outputSupervisionBehavior'             :  1365,
        'outputVoltageRiseRate'                 :  100,
        'outputVoltageFallRate'                 :  100,
        'outputUserConfig'                      :     8
    },
    'feb_p_side_ldo_1_2': {
       'ip': '10.203.0.62',
        'ch': 'u702',
        'outputSupervisionMaxCurrent'           :  4.01,
        'outputSupervisionMinSenseVoltage'      :  0.00,
        'outputSupervisionMaxSenseVoltage'      : 15.00,
        'outputSupervisionMaxTerminalVoltage'   :  8.50,
        'outputSupervisionMaxPower'             : 50.00,
        'outputVoltage'                         :  3.00,
        'outputCurrent'                         :  4.00,
        'outputSupervisionBehavior'             :  1365,
        'outputVoltageRiseRate'                 :  100,
        'outputVoltageFallRate'                 :  100,
        'outputUserConfig'                      :     8
    },
    'feb_p_side_ldo_1_8': {
        'ip': '10.203.0.62',
        'ch': 'u703',
        'outputSupervisionMaxCurrent'           :  4.01,
        'outputSupervisionMinSenseVoltage'      :  0.00,
        'outputSupervisionMaxSenseVoltage'      : 15.00,
        'outputSupervisionMaxTerminalVoltage'   :  8.50,
        'outputSupervisionMaxPower'             : 50.00,
        'outputVoltage'                         :  3.00,
        'outputCurrent'                         :  4.00,
        'outputSupervisionBehavior'             :  1365,
        'outputVoltageRiseRate'                 :  100,
        'outputVoltageFallRate'                 :  100,
        'outputUserConfig'                      :     8
    }

}

setup_command = [
    ['outputSupervisionMaxCurrent', 'F'],
    ['outputSupervisionMinSenseVoltage', 'F'],
    ['outputSupervisionMaxSenseVoltage', 'F'],
    ['outputSupervisionMaxTerminalVoltage', 'F'],
    ['outputSupervisionMaxPower', 'F'],
    ['outputVoltage', 'F'],
    ['outputVoltageRiseRate', 'F'],
    ['outputVoltageFallRate', 'F'],
    ['outputSupervisionBehavior', 'i'],
    ['outputUserConfig', 'i']
]

def sub_command_run(command):
    command_output = None
    try:
        logger.debug("Trying to: {}".format(command))
        command_output = subprocess.check_output(
            command,
            shell=True)
    except subprocess.CalledProcessError:
        print("Failed when trying: {}".format(command))

    return command_output.decode('utf-8').strip()

def channel_config(ch):
    for cfg, type in setup_command:
        command = "{} {} {}.{} {} {}".format(snmpset,channels[ch]['ip'],cfg,channels[ch]['ch'],type,channels[ch][cfg])
        sub_command_run(command)


def channel_on(ch):
    command = "{} {} outputSwitch.{} i 1".format(snmpset, channels[ch]['ip'], channels[ch]['ch']  )
    sub_command_run(command)

def channel_off(ch):
    command = "{} {} outputSwitch.{} i 0".format(snmpset, channels[ch]['ip'], channels[ch]['ch']  )
    sub_command_run(command)


def channel_voltage_set(ch):
    command = "{} {} outputVoltage.{}".format(snmpget, channels[ch]['ip'], channels[ch]['ch'])
    return sub_command_run(command)



def channel_voltage_sen(ch):
    command = "{} {} outputMeasurementSenseVoltage.{}".format(snmpget, channels[ch]['ip'], channels[ch]['ch'])
    return sub_command_run(command)


def channel_voltage_ter(ch):
    command = "{} {} outputMeasurementTerminalVoltage.{}".format(snmpget, channels[ch]['ip'], channels[ch]['ch'])
    return sub_command_run(command)


def channel_current(ch):
    command = "{} {} outputMeasurementCurrent.{}".format(snmpget, channels[ch]['ip'], channels[ch]['ch'])
    return sub_command_run(command)

def channel_status(ch) -> str:
    command = "{} {} outputStatus.{}".format(snmpget, channels[ch]['ip'], channels[ch]['ch'])
    status = sub_command_run(command)
    return "Status: {}\nSenseVoltage: {}\nTerminal Voltage:{}\nCurrent: {}\n".format(status, channel_voltage_sen(ch), channel_voltage_ter(ch), channel_current(ch))

channel_config('emu')
channel_on('emu')

while True:
    try:
        print (channel_status('emu'))
        time.sleep(1)
    except KeyboardInterrupt:
        break

channel_off('emu')
