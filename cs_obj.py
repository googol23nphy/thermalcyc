import epics
from epics import PV
import sys
from time import sleep
import json
""" CBM:STS Cooling System Object
    base class for cooling device communication
"""
class CoolingSysObj:
    set_temp_cmd=""
    get_temp_cmd=""
    get_sp_cmd=""
    set_point=0

    def __str__(self):
        return ("name: {}\nT_min: {}\nT_max: {}\n\n".format(self.device_name, self.temp_lower, self.temp_upper))

    def __init__(self, epics_device, temp_lower = -10, temp_upper = +10, temp_offset_= 2, tolerance = 1, config_file=None):

        self.device_name = epics_device
        self.temp_lower = temp_lower
        self.temp_upper = temp_upper
        self.temp_offset = temp_offset_
        self.tolerance = tolerance

    def reset(self):
        self.set_temp(self.get_room_temp())

    # Send message to change set point
    def set_temp(self, temp=20, wait=6, max_attempt=10):
        if temp > self.temp_upper or temp < self.temp_lower:
            raise UserWarning("Setting temperature outside ranges")

        self.setter = PV("{}:{}".format(self.device_name, self.set_temp_cmd), auto_monitor=True).put(value=temp, wait=True)
       
               
        attempts = 0
        #while temp != self.get_set_point() and attempts < max_attempt:
        #    self.setter
        #    attempts += 1
        #    sleep(wait)

        #if attempts == max_attempt:
        #    self.set_temp(temp=self.temp_upper)
        #    raise TimeoutError("could not establish set-point")
        #    sys.exit()

        self.set_point = temp


    def get_set_point(self):
        self.set_point = PV("{}:{}".format(self.device_name, self.get_sp_cmd), auto_monitor=True).value
        return self.set_point

    # Get current temperature
    def get_temp(self):
        self.temp_obj = PV("{}:{}".format(self.device_name, self.get_temp_cmd), auto_monitor=True).value
        return self.temp_obj

    # Return absolute difference between current temperature and set-point
    def get_offset(self):
        return abs(self.get_temp() - self.get_set_point())

    def is_set_point_reach(self):
        return self.get_offset() <= self.tolerance

    def get_room_temp(self):
        return self.temp_upper


""" CBM:STS lab cooling device: BINDER
    https://www.geass.com/wp-content/uploads/filebase/Binder/Camere-Climatiche-MK-MKT-MKF-MKFT/m/Manuale-Binder-KB.pdf
"""
class Binder(CoolingSysObj):
    def __init__(self, epics_device="CBM:STS:BINDER240", temp_lower = -10, temp_upper = +23, tolerance = 1, config_file=None):
        self.set_temp_cmd = "TSET"
        self.get_temp_cmd = "TEMP"
        self.get_sp_cmd = "TSET_VAL"
        self.tolerance = tolerance

        if config_file is not None:
            with open(config_file, "r") as json_file:
                loaded_data = json.load(json_file)

            self.device_name = loaded_data["binder_config"]["epics_device"]
            self.temp_lower = loaded_data["binder_config"]["temp_lower"]
            self.temp_upper = loaded_data["binder_config"]["temp_upper"]
        else:
            super().__init__(epics_device, temp_lower, temp_upper)

""" CBM:STS lab cooling device: LAUDA
    https://www.daeiltech.co.kr/wp-content/uploads/2021/02/New_ECO_Silver_Operating-manual.pdf
"""
class Lauda(CoolingSysObj):
    def __init__(self, epics_device="CBM:STS:LAUDA1", temp_lower = -20, temp_upper = +13, tolerance = 1, config_file=None):
        self.get_temp_cmd = "Read:Bath"
        self.set_temp_cmd = "Set00"
        self.get_sp_cmd = "Get00"
        self.tolerance = tolerance

        if not config_file == None:
            with open(config_file, "r") as json_file:
                loaded_data = json.load(json_file)

            self.device_name = loaded_data["lauda_config"]["epics_device"]
            self.temp_lower = loaded_data["lauda_config"]["temp_lower"]
            self.temp_upper = loaded_data["lauda_config"]["temp_upper"]
        else:
            super().__init__(epics_device, temp_lower, temp_upper)
