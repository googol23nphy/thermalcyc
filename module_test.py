import os
import sys
import json
import argparse
import time
import logging
import epics
from epics import PV
from datetime import datetime
import subprocess
import pprint

import tabulate as tb

from setup import *
from ps_obj import *

for root, dirs, files in os.walk("/home/cbm/cbmsoft/emu_test_module/"):
    for dir in dirs:
        path = os.path.join(root, dir)
        sys.path.append(path)

smx_library_status = True
try:
    import uhal
    import agwb
    from smx_tester import *
    import msts_defs as smc
    import smx_oper as smxoper
    #import test_setup as setup
    #from opm import Opm
except ModuleNotFoundError:
    print ("Module test library not found. Test cannot be done")
    smx_library_status = False

# Global variables
global_table_format = "psql"
global_smx_list = [[],[]]
global_output_path = "output/"
global_test_module = StsModule()

#active_downlinks = [1,2] #For modules PB
active_downlinks = [0,3]  #For modules PA

class TaskBase:
    def __init__ (self, task_name = "", config = {}):
        self.config = config
        self.name = task_name
        self.results = ""

        # Create a logger
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)  # Set the root logger level to DEBUG

        # Create logging folder
        self.log_folder = "log/"
        if not os.path.isdir(self.log_folder):
            os.makedirs(self.log_folder)

        # Create handlers for different log levels
        debug_handler = logging.FileHandler("log/{}_debug.log".format(self.__class__.__name__))
        debug_handler.setLevel(logging.DEBUG)

        warning_handler = logging.FileHandler("log/{}_warnings.log".format(self.__class__.__name__))
        warning_handler.setLevel(logging.WARNING)

        info_handler = logging.FileHandler("log/{}_info.log".format(self.__class__.__name__))
        info_handler.setLevel(logging.INFO)

        # Create formatters and add them to the handlers
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        debug_handler.setFormatter(formatter)
        warning_handler.setFormatter(formatter)
        info_handler.setFormatter(formatter)

        # Add handlers to the logger
        self.logger.addHandler(debug_handler)
        self.logger.addHandler(warning_handler)
        self.logger.addHandler(info_handler)

    def __str__(self) -> str:
        if len(self.config) == 0:
            return f"{self.__class__.__name__} task: Empty config"

        headers = [key for key in self.config]
        table_data = []
        table_data.append(self.config[key] for key in self.config)

        # Print the table
        str_table = "\n" + tb.tabulate(table_data, headers=headers, tablefmt=global_table_format)

        return f"{self.__class__.__name__} task: {str_table}"

# It establish connection with EMU boards, finding FEBs and synchronizing ASICs
class FullSync(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)

        try:
            self.emu_list = config['emu_list']
            self.link_scan = config['link_scan']
            self.device_scan = config['device_scan']
            self.link_sync = config['link_sync']
        except KeyError as e:
            self.logger.error("No configuration provided for {}".format(e))


    # def general_sync(): << Previous name
    def run(self):
        global global_smx_list
        global active_downlinks

        # Function to establish connection with EMU boards, finding FEBs and synchronizing ASICs
        manager = uhal.ConnectionManager("file://devices.xml")
        # Open xml file with list of EMU devices

        # Arrays to organize setup elements (FEBs)
        setup_elements = []
        emu_elements = []

        for emu in self.emu_list:
            ipbus_interface = IPbusInterface(manager, emu)
            agwb_top = agwb.top(ipbus_interface, 0)
            smx_tester = SmxTester(agwb_top, CLK_160)
            setup_elements_tmp = smx_tester.scan_setup()
            self.logger.info("setup_elements_tmp: %d", len(setup_elements_tmp))
            setup_elements.extend(setup_elements_tmp)
            emu_elements.extend( [emu for i in range(len(setup_elements_tmp))])

        for se in list(setup_elements):
            self.logger.info(f"SE:\t{se.downlink}\t\t{se.uplinks}  ({len(se.uplinks)})")
            if not se.downlink in active_downlinks:
                setup_elements.remove(se)
                self.logger.info(f"Remove setup element for downlink no. {se.downlink} with uplinks {se.uplinks}")

        for se in setup_elements:
            self.logger.info(f"Cleaned - SE:\t{se.downlink}\t\t{se.uplinks}  ({len(se.uplinks)})")

        if self.link_scan:
            for se in setup_elements:
                se.characterize_clock_phase()
                self.logger.info(f"post char clk phase\n {se}")
                se.initialize_clock_phase()
                self.logger.info(f"post set\n {se}")

            for se in setup_elements:
                se.characterize_data_phases()
                self.logger.info(f"post char data phase\n {se}")
                se.initialize_data_phases()
                self.logger.info(f"post set\n {se}")

        if self.device_scan:
            for se in setup_elements:
                se.scan_smx_asics_map()
                self.logger.info(f"post scan map|n {se}")

        if self.link_sync:
            for se in setup_elements:
                se.synchronize_elink()

            for se in setup_elements:
                se.write_smx_elink_masks()

        for se, emu in zip(setup_elements, emu_elements):
            smxes_tmp = smxes_from_setup_element(se)
            self.logger.info("smxes_tmp: %d", len(smxes_tmp))
            for smx in smxes_tmp:
                smx.rob = int(emu[4:])

                self.logger.info("Asic (emu %d   downlink %d   hw_addr %d): ", smx.rob, smx.downlink, smx.address)
                smx.func_to_reg(smc.R_ACT)
                smx.write_reg_all()
                smx.read_reg_all(compFlag = False)

                if (smx.downlink == 0 or smx.downlink == 1):
                    global_smx_list[0].extend(smx)
                else:
                    global_smx_list[1].extend(smx)

        self.logger.info("Number of setup elements: %d", len(setup_elements))
        self.logger.info("Number of smx n_side: %d", len(global_smx_list[0]))
        self.logger.info("Number of smx p_side: %d", len(global_smx_list[1]))
        return global_smx_list

class StdConfig(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)

    def run(self):
        global global_smx_list
        self.logger.info(f"running {self.__class__.__name__}")
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                self.logger.info("SETTING STANDARD CONFIGURATION for ASIC with HW address {} and polarity {}".format(smx.address,pol_str))
                smx.write_def_ana_reg(smx.address, side )
                smx.read_reg_all(compFlag = False)

# It reads the overall ASIC ID (FEB where ASIC belongs, polarity, HW address, ASIC e-fuse ID(string) and (int))
class ReadAsicIds(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.table_header = ["FEB_ID", "POLARITY", "ASIC_HW_ADDRESS", "ASIC_EFUSE_ID_(STR)", "ASIC_EFUSE_ID_(INT)"]
        self.table = pd.DataFrame(columns=self.table_header)

    def run(self):
        global global_smx_list
        self.logger.info(f"running {self.__class__.__name__}")
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                addr = smx.address
                asic_id_int = smx.read_efuse()
                asic_id_str = smx.read_efuse_str()
                self.table.loc[len(self.table)] = [global_test_module.feb_list[side], pol_str, addr, asic_id_str, asic_id_int]

        str_table = f"\n" + tb.tabulate(self.table, headers=self.table_header, tablefmt=global_table_format)
        self.logger.info(str_table)

        with open(f'{self.__class__.__name__}.dat', 'w+') as file:
            file.write(str_table)

# Function to read the VDDM and TEMP of the ASICs
class MonitorAsic(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.table_header = ["FEB_ID",
                             "POLARITY",
                             "ASIC_HW_ADDRESS",
                             "ASIC_VDDM (LSB)", "ASIC_VDDM (mV)",
                             "TEMPERATURE (LSB)", "TEMPERATURE (C)"]
        self.table = pd.DataFrame(columns=self.table_header)

    def run(self):
        global global_smx_list
        self.logger.info(f"running {self.__class__.__name__}")
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                addr = smx.address
                asic_vddm = smx.read_vddm()
                asic_vddm_src = smx.read_diag("Vddm")
                asic_temp = smx.read_temp()
                asic_temp_src = smx.read_diag("Temp")

                self.table.loc[len(self.table)] = [global_test_module.feb_list[side],
                                                   pol_str,
                                                   addr,
                                                   asic_vddm_src[1], asic_vddm_src,
                                                   asic_temp_src[0], asic_temp]

        str_table = f"\n" + tb.tabulate(self.table, headers=self.table_header, tablefmt=global_table_format)
        self.logger.info(str_table)
        with open(f'{self.__class__.__name__}.dat', 'w+') as file:
            file.write(str_table)

class ReadLV(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.feb_chan_map = config["feb_chan_map"]
        self.power_supply = PowerSupply(epics_device=config["PowerSupply"])
        self.table_header = ["LDO", "Voltage [V]", "Current [A]"]
        self.table = pd.DataFrame(columns=self.table_header)

    def run(self):
        self.logger.info(f"running {self.__class__.__name__}")

        for channel in self.power_supply.channels.keys():
            if "ldo" in channel:
                current, voltage = self.power_supply.get_iv(channel)
                self.table.loc[len(self.table)] = [channel, current, voltage]

        str_table = f"\n" + tb.tabulate(self.table, headers=self.table_header, tablefmt=global_table_format)
        self.logger.info(str_table)
        with open(f'{self.__class__.__name__}.dat', 'w+') as file:
            file.write(str_table)

class SetTrimDefault(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.trim_default = config["trim_default"]

    def run(self):
        global global_smx_list
        self.logger.info(f"running {self.__class__.__name__}")
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                asic_hw_addr = smx.address
                if asic_hw_addr in global_test_module.asic_cal_list[side]:
                    smx.set_trim_default(self.trim_default[0],self.trim_default[1])
                    self.logger.info("SETTING DEFAULT TRIM for ASIC with HW address {} and polarity {}".format(smx.address, pol_str))
                else:
                    self.logger.info("NO SETTING DEFAULT TRIM for ASIC with HW ADDRESS {} in {}".format(asic_hw_addr, pol_str))

class CheckConnection(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.v_ref_t_low = config["v_ref_t_low"]
        self.n_loops = config["n_loops"]

    def run(self):
        global global_smx_list
        self.logger.info(f"running {self.__class__.__name__}")
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                conn_dir = global_output_path + "/" + global_test_module.module_id
                if not os.path.isdir(conn_dir):
                    os.makedirs(conn_dir)

                asic_hw_addr = smx.address
                if asic_hw_addr in global_test_module.asic_cal_list[side]:
                    # Checking ENC and calibration results for an ASIC
                    self.logger.info(f"CONNECTION-CHECK SCAN for ASIC with HW ADDRESS {asic_hw_addr} in {pol_str}")
                    smx.connection_check(conn_dir, side, self.n_loops, self.v_ref_t_low)

class Calibration(TaskBase):
    def __init__(self, config = {}):
        super().__init__(self.__class__.__name__, config)
        self.test_ch = config["test_ch"]
        self.n_of_pulses = config["n_of_pulses"]
        self.amp_cal_min = config["amp_cal_min"]
        self.amp_cal_max = config["amp_cal_max"]
        self.amp_cal_fast = config["amp_cal_fast"]
        self.v_ref_t = config["v_ref_t"]
        self.much_mode_on = config["much_mode_on"]
        self.asic_cal_set = [], []

        self.table_header = ["SIDE"
                             "HW_ADDR",
                             "V_ref_P",
                             "V_ref_N",
                             "V_ref_T",
                             "V_ref_T_range",
                             "Threshold"]
        self.table = pd.DataFrame(columns=self.table_header)

    def run(self):
        global global_smx_list
        global global_test_module

        trim_dir = global_output_path + "/" + global_test_module.module_id + "/trim_files/"
        if not os.path.isdir(trim_dir):
            os.makedirs(trim_dir)

        for side in [0,1]:
            pol_str = 'elect' if side == 0 else 'holes'
            trim_final = [[0]*32 for _ in range(128)]
            for smx in global_smx_list[side]:
                asic_hw_addr = smx.address

                if asic_hw_addr in global_test_module.asic_cal_list[side]:

                    self.logger.info(f"RUNNING CALIBRATION FOR ASIC with HW ADDRESS {asic_hw_addr} in {pol_str}")

                    # Previously: scan_VrefP_N_Thr2glb
                    self.logger.info("runnning v_refs scan")
                    v_ref_p, v_ref_n, glb_thr = smx.vrefpn_scan(side, self.test_ch, self.n_of_pulses, self.amp_cal_min, self.amp_cal_max, self.amp_cal_fast, self.v_ref_t)
                    self.asic_cal_set[side].append([v_ref_p, v_ref_n, glb_thr])

                    self.logger.info("Writing calibration settings")
                    smx.write(130,  7, glb_thr)
                    smx.write(130,  9, v_ref_n)
                    smx.write(130,  8, v_ref_p)
                    smx.write(130, 18, self.v_ref_t)
                    v_ref_t_range  = (smx.read(130,10)&64)>>4 or (smx.read(130,18)&192)>>6

                    asic_id_str = smx.read_efuse_str()
                    v_ref_n = smx.read(130,8)&0xff
                    v_ref_p = smx.read(130,9)&0xff
                    v_ref_t = smx.read(130,18)&0xff
                    glb_thr = smx.read(130,7)&0xff

                    self.table.loc[len(self.table)] = [ pol_str,
                                                        asic_hw_addr,
                                                        v_ref_p,
                                                        v_ref_n,
                                                        v_ref_t,
                                                        v_ref_t_range,
                                                        glb_thr]

                    # Build trim file_name
                    filename_str = f"ftrim_{asic_id_str}_HW_{asic_hw_addr}_SET_{v_ref_p}_{v_ref_n}_{v_ref_t}_{glb_thr}_R_{self.amp_cal_min}_{self.amp_cal_max}_{pol_str}"
                    filename_trim = trim_dir + filename_str

                    self.logger.info(f"Calibration file name: {filename_trim}")

                    self.logger.info("Getting trim")
                    smx.get_trim_adc_SA(side, trim_final, 40, self.amp_cal_min, self.amp_cal_max, self.much_mode_on)
                    #smx.get_trim_adc(side, trim_final, 40, amp_cal_min, amp_cal_max,v_ref_t,  much_mode_on)
                    smx.get_trim_fast(side, trim_final, self.n_of_pulses, self.amp_cal_fast, self.much_mode_on)
                    # Writing calibration file
                    smx.write_trim_file(filename_trim, side, trim_final, self.amp_cal_min, self.amp_cal_max, self.amp_cal_fast, self.much_mode_on)
                else:
                    self.logger.info(f"SKIP_ASIC_HW_ADDR_{asic_hw_addr}")
                    pass

        str_table = f"\n" + tb.tabulate(self.table, headers = self.table_header, tablefmt = global_table_format)
        self.logger.info(str_table)
        with open(f'{self.__class__.__name__}.dat', 'w+') as file:
            file.write(str_table)

class CheckTrim(TaskBase):
    def __init__(self, task_name="", config={}):
        super().__init__(task_name, config)

        self.p_scan_dir
        self.disc_list = [5, 10, 16, 24, 30, 31]
        self.vp_min = 0
        self.vp_max = 255
        self.vp_step = 1
        self.n_of_pulses = 100

        self.table_header = ["SIDE",
                             "HW_ADDR",
                             "V_REF_T",
                             "THR_2_GLB",
                             "ENC_ADC",
                             "ENC_ADC_SIGMA",
                             "THR_ADC"
                             "THR_ADC_SIGMA"
                             "GAIN_ADC",
                             "GAIN_ADC_SIGMA",
                             "ENC_FAST",
                             "ENC_FAST_SIGMA",
                             "THR_FAST",
                             "THR_FAST_SIGMA"
                             ]
        self.table = pd.DataFrame(columns=self.table_header)

    def fit_s_curve(self, file_name = None):
        command = "root -l -q -b {script_name}(\"{file_name}\")"
        os.sys(command)
        return

    # Function to measure the ADC and FAST discriminator response function for a  given number of discriminators of an ASIC
    def run(self):
        for side in [0,1]:
            pol_str = 'n_side' if side == 0 else 'p_side'
            for smx in global_smx_list[side]:
                asic_hw_addr = smx.address
                if asic_hw_addr in global_test_module.asic_cal_list[side]:
                    # Checking ENC and calibration results for an ASIC
                    self.logger.info(f"PSCAN_ASIC_HW_ADDR_{asic_hw_addr}: {pol_str}")
                    # Elements for the pscan file
                    asic_id_str = smx.read_efuse_str()
                    p_scan_file_name = smx.check_trim_red(self.p_scan_dir, side, asic_id_str, self.disc_list, self.vp_min, self.vp_max, self.vp_step, self.n_of_pulses)
                    self.fit_s_curve(p_scan_file_name)
                    info = f"PSCAN_ASIC_HW_ADDR_{asic_hw_addr}: {p_scan_file_name}"

                else:
                    self.logger.info(f"SKIP_PSCAN_ASIC_HW_ADDR_{asic_hw_addr}")

class ModuleTest(TaskBase):
    def __init__ (self, config_file = ""):
        super().__init__(self.__class__.__name__, config_file)
        self.task_list = []
        self.output = []

        with open(config_file, "r") as json_file:
                config_info = json.load(json_file)

        global global_test_module
        global_test_module = StsModule(config=config_info["StsModule"])
        self.logger.info(global_test_module)

        for task in config_info["task_list"]:
            try:
                class_type = getattr(__import__(__name__), task)
                self.task_list.append(class_type(config_info["task_list"][task]))
                self.logger.info(f"Adding task: {self.task_list[-1]}")
            except AttributeError as type_name:
                self.logger.warning(f"Type '{type_name}' not found.")

        self.power_supply = PowerSupply(config_file=config_file)

    def run(self):
        # self.power_supply.power_all()

        for task in self.task_list:
            task.run()

# Example Usage
if __name__ == "__main__":
    test = ModuleTest("module_test_config.json")
    # test.run()

