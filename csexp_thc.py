
#import hmp4040

import epics
from epics import PV
import time
import os
import subprocess
import math
import logging
from subprocess import call
import tabulate as tb
from pathlib import Path
import numpy as np
import datetime
import sys
#SER=VCP109667 hmp_1  /dev/ttyACM0
#SER=VCP109659 hmp_2  /dev/ttyACM1
#General outline of HAMEG Power Supply PVs HMP4040:hmp_1:CH:1:Imom

#ColdStartup experiment class
class Experiment:

    #+++++++++++++++++++++++++++++++++++++++++Logger set up+++++++++++++++++++++++++++++++++++++++++++++++++
    logger = logging.getLogger(__name__)
    date_time = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    log_file_name = '{}_run_exp.log'.format(date_time)
    logging.basicConfig(filename=log_file_name)
    logger.setLevel(logging.DEBUG)
    file_formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s%(message)s')
    stream_formatter = logging.Formatter('%(message)s')

    file_handler = logging.FileHandler('coldStartup.log')
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(file_formatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(stream_formatter)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    logger1 = logging.getLogger("Otherlogger")
    logger1.setLevel(logging.DEBUG)

    file_handler1 = logging.FileHandler('cycleRecord.log')
    file_handler1.setLevel(logging.DEBUG)
    file_handler1.setFormatter(file_formatter)
    logger1.addHandler(file_handler1)


    #++++++++++++Creating necessary variables, can skip directly after __init__ to see methods++++++++++++++++++++++++++
    # FEB names
    feb_name_list = []
    feb_test_result = []
    feb_result_table = {}
    feb_result_table_columns = ["Thermal Cycle", "Power Cycle", "ASIC", "Temperature", "Vddm", "CSA"]
    power_consumption_result = []
    consumption_result_table = {}
    consumption_result_table_columns = ["Volatge[V](1.2V)", "Current[A](1.2V)", "Voltage[V](1.8V)", "Current[A](1.8V)"]
    module_ID = ""
    moduleID_path = "{}/".format(module_ID)

    th_cycle_idx = -1
    ps_cycle_idx = -1

    # Full list of PV names as external database file
    pv_base_name = 'coldstartupbase.dbl'
    pv_base_lst = [] #set of PV-s generated from the file

    # +++++++++++++++++++++++++++++++++++++++++++ Power Supply Variables +++++++++++++++++++++++++++++++++++++++++++++++
    #Ch VorC   = [CV1A, CC1A, CV2D, CC2D, CV3D, CC3D, CV4A, CC4A]  # Power Supply preset settings - Channel: Voltage / Current
    #Ch VorC   = [CV1A, CC1A, CV2D, CC2D, CV3D, CC3D, CV4A, CC4A]
    ps_values_on = [2.5, 3.1, 2.8, 3.1, 2.5, 3.1, 2.8, 3.1]  # hmg_2 lower active for module test
                  #[2.6, 3.1, 2.2, 3.1, 2.2, 3.1, 2.6, 3.1]  # hmp_3 higher

    psvaluesoff = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  #hmp_1
                  #0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]  #hmp_2
    # #Power supply details
    devserlst = ['SER=VCP109667  /dev/ttyACM0', #hmp_1
                 'SER=VCP109659  /dev/ttyACM1'] #hmp_2

    #++++++++++++++++++++++++++++++++++++ Binder and Lauda Variables +++++++++++++++++++++++++++++++++++++
    #Specify lower and higher cycling temperatures
    binder_temp_min = -20 # Binder Temperature (Lauda = Binder +2)
    binder_temp_max = +20

    # Number of Power cycles
    n_ps_cycles_at_max_temp = 1
    n_ps_cycles_at_min_temp = 5

    # Number of Thermal cycles
    n_thermal_cycles = 1

    #Temperature sensor stability, Lauda & Binder temperature tolerance from set values
    binder_lauda_delta_temp = 1
    tempToleranceSensors = 2.0
    tempToleranceLaudaBinder = 2.0
    sensors_wait_time_sec = 60 # defines the time program waits for the stability of Temperature sensor, units: Seconds
    cooling_wait_time_sec = 60 # defines the time program waits for the stability of Lauda & Binder values, units: Seconds
    stability_time_temp_max = 300
    stability_time_temp_min = 900
    cooling_wait_time_min_to_max = 3600
    cooling_wait_time_max_to_min = 3600 # time to wait for the Binder and Lauda to reach binder_temp_min from binder_temp_max units: Seconds
    cool_feb_wait_time = 180

    #+++++++++++++++++++++++++++++++++Creating PV (Process Variables with epics) ++++++++++++++++++++++++++++++++++
    # PVs definitions Lauda and Chamber
    lauda_temp = PV("CBM:STS:LAUDA1:Read:Bath", auto_monitor=True) # HARDCODE
    lauda_set = PV("CBM:STS:LAUDA1:Set00") # HARDCODE

    # PvS definitions Binder
    binder_temp = PV("CBM:STS:BINDER240:TEMP", auto_monitor=True) # HARDCODE
    binder_set = PV("CBM:STS:BINDER240:TSET") # HARDCODE
    binder_set_val = PV("CBM:STS:BINDER240:TSET_VAL")
    # Fin Temperature Sensor PVs
    tempSens1 = PV("CBM:STS:TD:DM:0:TEMP1") # HARDCODE
    tempSens2 = PV("CBM:STS:TD:DM:0:TEMP2") # HARDCODE
    tempSens3 = PV("CBM:STS:TD:DM:0:TEMP3") # HARDCODE

    # Generating full list of PVs from the basefile
    def createpvfromBase(self):
        try:
            f = open(self.pv_base_name, "r", encoding="utf-8")
            for i in f:
                self.pv_base_lst.append(PV(i.strip('\n')))

        finally:
            f.close()
    #++++++++++++++++++++++++++++++++++++++++++++++Main Init - METHODS++++++++++++++++++++++++++++++++++++++++++++++++++++++
    def __init__(self):
        #Creating PV list from database
        self.createpvfromBase()



    # This function perform power supply cycle FEB tests at the extremes of the cooling system temperatures ranges:
    # n_ps_cycles_at_min_temp at cooling system min temp
    # n_ps_cycles_at_max_temp at cooling system max temp
    # A power supply cycle FEB test consists on:
    # - Switch OFF the power supply
    # - Switch ON the power supply
    # - Run FEB configuration
    # - Switch off the power supply
    # It will go from max-min-max temperature n_thermal_cycles times (thermal cycles)
    # At the end of the thermal cycling, an output files is save for each tested FEB containing:
    # thermal cycle idx, power supply cycle idx, ASIC idx, ASIC temperature, ASIC Vddm, ASIC CSA
    def csRun(self):
        self.feb_name_list.append(input("Introduce FEB_ID n-side: "))
        self.feb_name_list.append(input("Introduce FEB_ID p-side: "))
        for feb_name in self.feb_name_list:
            # Create the result table for each FEB
            self.feb_result_table[feb_name] = np.zeros(shape=[0,6])


        for th_cycle in range(self.n_thermal_cycles): # thermal cycles loop
            self.th_cycle_idx = th_cycle

            self.logger.info(" Performing thermal cycle {}\n".format(th_cycle))

            # Switch OFF power supply
            self.psOff()

            # Setting up cooling system
            if self.setCoolingSysT(self.binder_temp_max) == -1:
                self.logger.info("Cooling system could not reach set temperature\n")
                self.resetCoolingSysT()
                return -1

            self.WaitClock(wait_time=self.stability_time_temp_max, message="Waiting to stabilize the Binder and Lauda temperatures")

            self.logger.info(" Running {} FEB tests at {}\n".format(self.n_ps_cycles_at_max_temp,self.binder_temp_max))

            self.power_cycle(self.n_ps_cycles_at_max_temp)

            # Set min temp to cooling system
            if self.setCoolingSysT(self.binder_temp_min) == -1:
                self.logger.warning("Cooling system could not reach set temperature\n")
                self.resetCoolingSysT()
                return -1

            self.WaitClock(wait_time=self.stability_time_temp_min, message="Waiting to stabilize the Binder and Lauda temperatures")

            self.logger.info(" Running {} FEB tests at {}\n".format(self.n_ps_cycles_at_min_temp,self.binder_temp_min))

            self.power_cycle(self.n_ps_cycles_at_min_temp)

        # end of thermal cycles

        self.RefreshFiles()

        self.logger.info("Setting all to the warm temperatures\n")
        self.resetCoolingSysT()
        self.logger.info("Switching off power supplies\n")
        self.psOff()
        self.logger.info(" END OF EXPERIMENT, Hooray!\n")

        return 1

    # Perform n_cycles amount of power supply test
    def power_cycle(self, n_cycles):
        for ps_cycle in range(n_cycles):
            self.ps_cycle_idx = ps_cycle
            self.logger.info(" ... test {}...\n".format(ps_cycle))
            self.power_supply_test()

    # Perform PS test:
    # 1. Configure
    # 2. Switch-On
    # 3. Run FEB tests
    # 4. Switch-Off
    # 5. Wait for FEB to thermalize
    def power_supply_test(self):
        # Configure and power ON power supply
        self.psSettings()
        self.psOn()

        # Configure FEB(s) (run FEB(s) test)
        self.RunFebTest()
        # Switch OFF power supply
        self.psOff()


        time.sleep(self.cool_feb_wait_time)

    # Write to files the current status of the run store into feb_result_table:
    # thermal cycle idx, power supply cycle idx, ASIC idx, ASIC temperature, ASIC Vddm, ASIC CSA
    def RefreshFiles(self):
        # Write result table to files for each FEB
        for feb_name in self.feb_name_list: # FEB loop
            # Reshape the array to be a table like
            self.feb_result_table[feb_name] = np.reshape(self.feb_result_table[feb_name], (-1,6))

            # Create & Open output file
            output_file = open(("{}/{}_result.info").format(self.module_ID,feb_name),"w")

            # Write the table using tabular formatting
            output_file.write(tb.tabulate(self.feb_result_table[feb_name], headers=self.feb_result_table_columns))
        # end FEB loop

    #+++++++++++++++++++++++++++++++++++++++++Power Supply METHODS+++++++++++++++++++++++++++++++++++++++++++++++++

    # set the desired values of Power Supply settings in the PV-s
    # see the list ps_values_on at the top of this script to change settings
    def psSettings(self, settings = ps_values_on):
        for indexofPSPVs in range( 10,18):
            if(self.containshmp(self.pv_base_lst[indexofPSPVs])):
                self.pv_base_lst[indexofPSPVs].put(settings[indexofPSPVs-10])
                self.logger.debug('{} {}'.format(self.pv_base_lst[indexofPSPVs], settings[indexofPSPVs-10]))
            else:
                break
        self.logger.debug(" Settings are set for Power Supply\n")

    # Set State, Voltage, Current on power supplies to zero
    def psOff(self):
        for indexofPSPVs in range(0,10):
            if(self.containshmp(self.pv_base_lst[indexofPSPVs])):
                self.logger.debug('{} {}' .format(self.pv_base_lst[indexofPSPVs], 0))
                self.pv_base_lst[indexofPSPVs].put(0)

        self.logger.debug(" PS Switched off\n")

    #Set State, Voltage, Current on power supplies to one
    def psOn(self):
        for indexofPSPVs in range(0,10):
            if(self.containshmp(self.pv_base_lst[indexofPSPVs])):
                self.logger.debug('{} {}' .format(self.pv_base_lst[indexofPSPVs], 1))
                self.pv_base_lst[indexofPSPVs].put(1)
            else:
                break
        self.logger.debug(" PS Switched on\n")

    # To make sure we are not changing non hmp related PV, returns True/False
    def containshmp(self, pv):
        if 'HMG' in str(pv):
            return True
        else:
            self.logger.error(" ERROR: This PV name does not contain 'hmp' in it, there is a risk of changing  Power Supply not related PV!\n")
            return False

    #++++++++++++++++++++++++++++++++++++++++++++Lauda and Binder Methods+++++++++++++++++++++++++++++++++++++++++++++++++++
    # Custom countdown clock
    def WaitClock(self, wait_time, dt=1, message=""):
        self.logger.info("{} : Waiting {} sec  ".format(message, int(wait_time)))
        for i in range(int(wait_time)):
            time_left = wait_time - i*dt
#            print("{} : {} sec left ".format(message, int(time_left)), end="\r", flush=True)
            print("{} : {} sec left ".format(message, int(time_left)), flush=True, end="\r")
            time.sleep(dt)

    # Returns current Binder temperature
    def getBinderT(self):
        return self.binder_temp.value

    # Returns current Lauda temperature
    def getLaudaT(self):
        return self.lauda_temp.value

    # Set Binder temperature set-point
    def setBinderT(self, temp):
        self.binder_set.put(value=temp, wait=True)
        attempts = 0
        while self.binder_set.get() != self.binder_set_val.get() and attempts < 10:
            self.binder_set.put(value=temp, wait=True)
            attempts += 1
        if attempts == 10:
            self.resetCoolingSysT()
            sys.exit()

    # Set Lauda temperature set-point
    def setLaudaT(self, temp):
        self.lauda_set.put(value = temp, wait=True)

    def resetCoolingSysT(self):
        self.setBinderT(self.binder_temp_max)
        self.setLaudaT(self.binder_temp_max+1)

    def setCoolingSysT(self, temp):
        n_intervals = 1
        start_temp_binder = self.getBinderT()
        start_temp_lauda = self.getLaudaT()

        end_temp_binder = temp
        end_temp_lauda = end_temp_binder + self.binder_lauda_delta_temp

        d_temp_binder = (end_temp_binder - start_temp_binder) / n_intervals
        d_temp_lauda  = (end_temp_lauda - start_temp_lauda) / n_intervals

        cooling_rate_binder = 50 # sec / °C

        self.logger.info("Setting Cooling System Temperatures:\n\tLauda: {}°C\n\tBinder: {}°C\n".format(end_temp_lauda,end_temp_binder))

        for n in range(1, n_intervals+1, 1):
            t_binder = start_temp_binder + d_temp_binder * n
            t_lauda  = start_temp_lauda  + d_temp_lauda  * n

            t_0 = time.time()
            while abs(self.getBinderT() - t_binder) > self.tempToleranceLaudaBinder or abs(self.getLaudaT() - t_lauda) > self.tempToleranceLaudaBinder :
                self.logger.info("Setting Binder Temperatures to: {}\n".format(t_binder))
                self.setBinderT(t_binder)
                self.logger.info("Setting Lauda Temperatures to: {}\n".format(t_lauda))
                self.setLaudaT(t_lauda)

                # Wait to reach set temperature
                w_time = cooling_rate_binder*max(abs(d_temp_binder),abs(d_temp_lauda))
                self.WaitClock(wait_time=w_time, message="Waiting to reach set temperature")

                # Check for maximum wait time
                t_1 = time.time()
                if t_1-t_0 > 3*w_time:
                    self.logger.warning("Cooling time limit exceeded: {}\n".format(3*w_time))
                    return -1

    #++++++++++++++++++++++++++++++++++++++++++++ FEB testing +++++++++++++++++++++++++++++++++++++++++++++++++++
    # should return an array for each FEB tested containing 8 set of Vdd, temp, csa values
    def RunFebTest(self):
        test_script = "/home/cbm/cbmsoft/emu_tud_feb8/sw/run/tstModuleAssembly1.py" # HARDCODE
        time.sleep(10)
        for feb_name in self.feb_name_list:
            feb_sn = int(feb_name[0:len(feb_name)-1])
            feb_type = feb_name[len(feb_name)-1]
            command = ("python  {} --feb_type {} --feb_row all --feb_sn {}").format(test_script, feb_type, feb_sn)
            os.system(command)

            # collect test_results: max 8 rows of v_ddm, csa, temp values
            path = Path(("/home/cbm/cbmsoft/emu_tud_feb8/sw/run/{}/{}{}_last_test.info").format(self.module_ID, feb_sn, feb_type))
            if path.is_file():
                result_file = np.loadtxt(("/home/cbm/cbmsoft/emu_tud_feb8/sw/run/{}/{}{}_last_test.info").format(self.module_ID, feb_sn, feb_type))
                for row in result_file: # each line corresponds to an ASIC
                    try:
                        chip_nr = row[0]
                        chip_temp = row[1]
                        chip_vddm = row[2]
                        chip_csa = row[3]
                    except IndexError:
                        print(('IndexError: File {} is malformed').format(path))
                    else:
                        self.feb_result_table[feb_name] = np.append(self.feb_result_table[feb_name], [self.th_cycle_idx, self.ps_cycle_idx, chip_nr, chip_temp, chip_vddm, chip_csa])
            else:
                self.logger.warning(('File {} does not exist').format(path))

                
        # Update of the output files
        self.RefreshFiles()

    #++++++++++++++++++++++++++++++++++++++++++++Lauda and Binder Methods+++++++++++++++++++++++++++++++++++++++++++++++++++

    # archives link for retrieving PVS: http://cbmdpb05:17665/retrieval/ui/viewer/archViewer.html?pv=CBM:STS:LAUDA1:Read:Bath&pv=CBM:STS:BINDER240:TEMP
