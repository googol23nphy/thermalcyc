
import pandas as pd 
import time
import numpy as np
import tabulate as tb
import os
from pathlib import Path
import logging
import datetime

from cs_obj import *
from ps_obj import *
from setup import *

import json

class Experiment:
    # Class self.logger config
    logger = logging.getLogger(__name__)
    date_time = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
    log_file_name = '{}_run_exp.log'.format(date_time)
    logging.basicConfig(filename=log_file_name)
    logger.setLevel(logging.DEBUG)
    #####################

    th_cycle_idx = -1
    ps_cycle_idx = -1
    result_table_columns = ["Thermal Cycle", "Power Cycle", "FEB_ID", "POLARITY", "ASIC", "VDDM_LSB", "VDDM", "TEMP_LSB", "Temperature"]
    result_table = {}

    module_tests = {
        "full_sync": True,
        "std_config": True,
        "read_asic_id": True,
        "set_trim_default": True,
        "read_lv_ac": True,
        "check_vddm_temp": True,
        "conn_check": False
    }

    def __init__(self, binder = None, lauda = None, power_supply = None, config_file="test_parameters.json"):
        if not config_file == None:
            exp_conf_file=config_file
            self.binder = Binder(config_file=config_file)
            self.lauda = Lauda(config_file=config_file)
            self.setup = Setup(config_file=config_file)
            self.power_supply = PowerSupply(config_file=config_file)

            with open(config_file, "r") as json_file:
                loaded_data = json.load(json_file)

            self.n_of_thermal_cycles = loaded_data["n_of_thermal_cycles"]
            self.n_of_power_cycles_at_lower = loaded_data["n_of_power_cycles"]["at_lower"]
            self.n_of_power_cycles_at_upper = loaded_data["n_of_power_cycles"]["at_upper"]
        else:
            if self.binder == None or self.lauda == None or self.power_supply == None:
                self.exp_status = -1
                raise RuntimeError

            self.binder = binder
            self.lauda = lauda
            self.power_supply = power_supply

        for sts_module in self.setup.module_list:
            self.result_table[sts_module.module_id] = pd.DataFrame(columns=self.result_table_columns)

        self.exp_datetime_start = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")

    def __str__(self):
        return "binder_config\n{}lauda_config\n{}power_supply_config\n{}".format(self.binder, self.lauda, self.power_supply)

    def ramp_cs(self, mode=-1, duration=3600, n_intervals=10):
        binder_target_temp = self.binder.temp_lower if mode == -1 else self.binder.temp_upper
        lauda_target_temp = self.lauda.temp_lower if mode == -1 else self.lauda.temp_upper

        start_temp_binder = self.binder.get_temp()
        start_temp_lauda = self.lauda.get_temp()

        d_temp_binder = (binder_target_temp - start_temp_binder) / n_intervals
        d_temp_lauda  = (lauda_target_temp - start_temp_lauda) / n_intervals
        cooling_rate_binder = 50 # sec / °C
        wait_time_per_interval = int(duration / n_intervals)
        for n in range(1, n_intervals):
            interval_start_time = time.time()
            interval_end_temp_binder = start_temp_binder + d_temp_binder * n
            interval_end_temp_lauda = start_temp_lauda   + d_temp_lauda * n
            self.logger.info("Setting Binder Temperatures to: {}\n".format(interval_end_temp_binder))
            print("Setting Binder Temperatures to: {}\n".format(interval_end_temp_binder))
            self.logger.info("Setting Lauda Temperatures to: {}\n".format(interval_end_temp_lauda))
            print("Setting Lauda Temperatures to: {}\n".format(interval_end_temp_lauda))

            while abs(self.binder.get_temp() - interval_end_temp_binder) > self.binder.tolerance or abs(self.lauda.get_temp() - interval_end_temp_lauda) > self.lauda.tolerance:
                self.lauda.set_temp(interval_end_temp_lauda)
                self.binder.set_temp(interval_end_temp_binder)

            t_0 = time.time()
            while not self.binder.is_set_point_reach() or not self.lauda.is_set_point_reach():

                # Wait to reach set temperature
                w_time = cooling_rate_binder*max(abs(d_temp_binder),abs(d_temp_lauda))
                self.wait_clock(wait_time=w_time, message="Waiting to reach set temperature")

                # Check for maximum wait time
                t_1 = time.time()
                if t_1-t_0 > 3*w_time:
                    print()
                    self.logger.error("Cooling time limit exceeded: {}\n".format(3*w_time))
                    self.cs_reset()
                    raise TimeoutError

            elapsed_time = time.time() - interval_start_time
            extra_time = wait_time_per_interval - elapsed_time
            if extra_time > 0:
                sleep(extra_time)

    def run(self):
        self.cs_reset()
        while not self.binder.is_set_point_reach() or not self.lauda.is_set_point_reach():
            self.wait_clock(wait_time=20)

        for th_cycle_idx in range(self.n_of_thermal_cycles):
            self.th_cycle_idx = th_cycle_idx
            self.ps_cycle_idx = 0

            self.logger.info(" Performing thermal cycle {}\n".format(th_cycle_idx))
            print(" Performing thermal cycle {}\n".format(th_cycle_idx))

            self.logger.info(" Running {} FEB tests at {}\n".format(self.n_of_power_cycles_at_upper,self.binder.temp_upper))
            print(" Running {} FEB tests at {}\n".format(self.n_of_power_cycles_at_upper,self.binder.temp_upper))
            self.power_cycle(self.n_of_power_cycles_at_upper)

            self.ramp_cs(mode=-1)
            self.wait_clock(wait_time=300)

            self.logger.info(" Running {} FEB tests at {}\n".format(self.n_of_power_cycles_at_lower,self.lauda.temp_lower))
            print(" Running {} FEB tests at {}\n".format(self.n_of_power_cycles_at_lower,self.lauda.temp_lower))
            self.power_cycle(self.n_of_power_cycles_at_lower)

            self.ramp_cs(mode=1)
            self.wait_clock(wait_time=300)

        self.logger.info("Setting the cooling system to room temperature\n")
        print("Setting the cooling system to room temperature\n")
        self.cs_reset()
        self.logger.info(" END OF EXPERIMENT, Hooray!\n")
        print(" END OF EXPERIMENT, Hooray!\n")


    def cs_reset(self):
        self.logger.info("Setting the cooling system to room temperature\n")
        print("Setting the cooling system to room temperature\n")
        self.binder.reset()
        self.lauda.reset()

    def wait_clock(self, wait_time, dt=1, message="Wait"):
        print("{} : {} sec  ".format(message, int(wait_time)))
        for i in range(int(wait_time)):
            time_left = wait_time - i*dt
            print("{} : {} sec left ".format(message, int(time_left)), flush=True, end="\r")
            time.sleep(2)

    def power_cycle(self, n_cycles):
        for ps_cycle in range(n_cycles):
            self.logger.info(" ... Power Cycle {}...\n".format(ps_cycle))
            print(" ... Power Cycle {}...\n".format(ps_cycle))

            # Config module tests
            if ((self.th_cycle_idx == 0 and self.ps_cycle_idx == self.n_of_power_cycles_at_upper) or
                (self.th_cycle_idx == self.n_of_thermal_cycles - 1 and self.ps_cycle_idx == self.n_of_power_cycles_at_upper + self.n_of_power_cycles_at_lower - 1)):
                self.module_tests["conn_check"] = True
            else:
                self.module_tests["conn_check"] = False

            with open("module_test_config.json", 'w') as json_file:
                json.dump(self.module_tests, json_file, indent=4)

            self.power_cycle_test()
            sleep(120)

            self.ps_cycle_idx += 1

    def power_cycle_test(self):
        self.power_supply.power_all(state=False)
        self.power_supply.set_iv()
        self.power_supply.power_all(state=True)
        self.logger.debug(" Power Supply Switched on\n")
        print(" Power Supply Switched on\n")
        self.setup.test()
        self.power_supply.power_all(state=False)
        self.logger.debug(" Power Supply Switched off\n")
        print(" Power Supply Switched off\n")

        self.refresh_files()

    def create_table(self, sts_module):
        table = sts_module.module_test_results
        table.insert(0, self.result_table_columns[0], self.th_cycle_idx)
        table.insert(1, self.result_table_columns[1], self.ps_cycle_idx)
        
        self.result_table[sts_module.module_id] = pd.concat([self.result_table[sts_module.module_id], table])

        return table

    def refresh_files(self):
        for sts_module in self.setup.module_list:
            self.create_table(sts_module)

            # Create & Open output file
            file_name = ("test_results/{}/{}_{}_result.info").format(sts_module.module_id, self.exp_datetime_start, sts_module.module_id)
            output_file = open(file_name,"w+")
            output_file.write(tb.tabulate(self.result_table[sts_module.module_id], headers=self.result_table_columns))

#exp = Experiment()
