import os
import time
import numpy as np
import json
import logging
import pandas as pd 

""" Dummy class for StsModule completeness """
class StsSensor:
    def __init__(self) -> None:
        pass

""" Dummy class for StsModule completeness """
class StsMicrocables:
    def __init__(self) -> None:
        pass

""" FEB test object """
class StsFeb:
    def __init__(self, feb_type=None,  feb_sn=None, module_id = None, feb_name = None):
        if feb_name == None:
            self.feb_type = feb_type
            self.feb_sn = feb_sn
            self.name = "{}{}".format(self.feb_sn,self.feb_type)
        else:
            self.name = feb_name
            self.feb_sn = feb_name[0:-2]
            self.feb_type = feb_name[-2]
            self.n_of_up_link = feb_name[-1]

        self.output_folder = "test_results/"

    def __str__(self) -> str:
        return f"type={self.feb_type}, SN={self.feb_sn}, version: {self.n_of_up_link}"

    def test(self, feb_test_script=""):
        if not os.path.isfile(feb_test_script):
            raise FileExistsError(feb_test_script)

        # env_status=os.system("source ~/cbmsoft/ipbuslogin.sh")
        command = ("python3  ../emu_test_module/python/feb_test.py")
        os.system(command)

        try:
            return self.read_results_from_file()
        except FileExistsError:
            self.logger
            return None

    def read_results_from_file(self):
        file_name = "{}{}_last_test.info".format(self.output_folder, self.module_id)
        if not os.path.isfile(file_name):
            raise FileExistsError(file_name)

        self.test_results = np.zeros(shape=[0,7])

        result_file = np.loadtxt(file_name)
        for row in result_file: # each line corresponds to an ASIC
            try:
                feb_id = row[0]
                polarity = row[1]
                chip_addr = row[2]
                chip_vddm_lsb = row[3]
                chip_vddm = row[4]
                chip_temp_lsb = row[5]
                chip_temp = row[6]
            except IndexError:
                print(('IndexError: File {} is malformed').format(file_name))
            else:
                self.test_results = np.append(self.test_results, [[feb_id, polarity, chip_addr, chip_vddm_lsb, chip_vddm,
                                                                    chip_temp_lsb, chip_temp]], axis=0)

        ## Add new column to result table as FEB name
        #new_column = np.full(self.test_results.shape[0], self.name)
        #self.test_results = np.hstack((new_column.reshape(-1, 1), self.test_results))
        return file_name


""" Sts Module obj """
class StsModule:
    def __init__(self, module_id=None, feb_list_name=[], sensor=None, micro_cables=None, config=None):
        if config == None:
            self.module_id = module_id
            self.output_folder = "test_results/"
            self.feb_list=[]

            for feb_name in feb_list_name:
                self.feb_list.append(StsFeb(feb_name=feb_name))

            if not sensor == None: self.sensor = sensor
            if not micro_cables == None: self.micro_cables = micro_cables

            self.asic_cal_list = [],[]
        else:
            self.module_id = config["module_id"]
            self.feb_list = [StsFeb(feb_name=config["feb_n_side"]), StsFeb(feb_name=config["feb_p_side"])]

            self.asic_cal_list = config["asic_cal_list"]

            if not sensor == None: self.sensor = sensor
            if not micro_cables == None: self.micro_cables = micro_cables


    def __str__(self) -> str:
        return f"Module_ID: {self.module_id}\t[feb_n_side: {self.feb_list[0]} ; feb_p_side: {self.feb_list[1]}]\n asic_cal_list:{self.asic_cal_list}"

    def test(self,feb_test_script=""):
        if not os.path.isfile(feb_test_script):
            raise FileExistsError(feb_test_script)

        if not os.path.isdir(self.output_folder):
            os.makedirs(self.output_folder)
        command = "python3 {} --module_id={} --feb_p_side={} --feb_n_side={} --output_path={}".format(feb_test_script, self.module_id, self.feb_list[0].name, self.feb_list[1].name, self.output_folder)
        os.system(command)

        results_file_name = "{}/{}_last_test.info".format(self.output_folder, self.module_id)
        header = ["FEB_ID", "POLARITY", "ASIC", "VDDM_LSB", "VDDM", "TEMP_LSB", "Temperature"]
        self.module_test_results  = pd.read_table(results_file_name, skiprows=1, names=header)

        # Make a copy inside
        folder_name = "{}/{}".format(os.path.dirname(results_file_name), self.module_id)
        destination_path = "{}/{}.info".format(folder_name, self.module_id)
        if not os.path.exists(folder_name):
            os.makedirs(folder_name)

        os.rename(results_file_name, destination_path)

        return self.module_test_results


""" Thermal cycling setup
    Collection of StsModules """
class Setup:
    # Class self.logger config
    logger = logging.getLogger(__name__)
    log_file_name = 'setup.log'
    logging.basicConfig(filename=log_file_name, format='%(asctime)s %(levelname)s %(message)s')
    logger.setLevel(logging.DEBUG)
    #####################

    def __init__(self, module_list=[], config_file=None):
        self.module_list = module_list

        if not config_file == None:
            with open(config_file, "r") as json_file:
                loaded_data = json.load(json_file)

            self.module_list.clear()
            for mod_config in loaded_data["modules"]:
                new_module = StsModule(mod_config["id"], [mod_config["feb_p_side"]["name"], mod_config["feb_n_side"]["name"]])
                self.module_list.append(new_module)

            self.test_script = loaded_data["feb_test_script"]

    def __str__(self) -> str:
        stream = ""
        for sts_module in self.module_list:
            print (sts_module)
        return stream

    def test(self, test_script=None):
        if test_script == None:
            test_script = self.test_script
        if not os.path.isfile(test_script):
            raise FileExistsError(test_script)

        for sts_module in self.module_list:
            self.logger.debug("Performing test for module: {}".format(sts_module))
            sts_module.test(test_script)

    def add_module(self, sts_module):
        self.module_list.append(sts_module)


# """ Single FEB test usage """
# sts_feb = StsFeb(feb_type="A", feb_sn=144)
# print (sts_feb)
# sts_feb.test("dummy_feb_test.py")


# """ Single module usage (FEB pair) """
# module_to_test = StsModule(module_id=1234,feb_list_name=["144A", "144B"])
# print (module_to_test)
# module_to_test.test("dummy_feb_test.py")


# """ Multiple module (complex setup) """
# module_to_test = [StsModule(module_id=1234,feb_list_name=["144A", "144B"]),
#                   StsModule(module_id=4321,feb_list_name=["144A", "144B"])]

# setup = Setup(module_list=module_to_test)
# print (setup)
# # setup.test("dummy_feb_test.py")
